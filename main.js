"use strict";

const titles = document.querySelectorAll(".tabs-title");
const titlesArray = [...titles];
const contents = document.querySelector(".tabs-content").children;
const contentsArray = [...contents];

function restartDefault() {
    titlesArray.forEach((element) => {
    element.classList.remove("active");

    contentsArray.forEach((element) => {
      element.style.display = "none";
    });
  });
}
restartDefault();

titlesArray.forEach((title) => {
  title.addEventListener("click", () => {
    contentsArray.forEach((element) => {
      if ( title.dataset.title === element.dataset.ferst) {
        restartDefault();
        title.classList.add("active");
        element.style.display = "block";
      }
    });
  });
});


